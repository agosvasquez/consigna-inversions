Dado("que mi cuit es {string}") do |cuit|
  Faraday.post "#{BASE_URL}/reset"
  @cuit = cuit
end

Dado("que compro $ {int} dólares a cotizacion $ {float}") do |capital_invertido, cotizacion_que_compre_dolar|
  @capital_invertido = capital_invertido
  @cotizacion_que_compre_dolar = cotizacion_que_compre_dolar
end

Dado("que el interes de plazo fijo es {int} % anual") do |interes_anual|
  @interes_anual = interes_anual
end

Dado("que compro $ {float} en acciones a $ {float} por acción") do |capital_invertido, valor_por_accion_comprada|
  @capital_invertido = capital_invertido
  @valor_por_accion_comprada = valor_por_accion_comprada
end

Cuando("vendo esos dolares a cotización $ {float}") do |cotizacion_que_vendi_dolar|
  body = {"capital": @capital_invertido, "cotizacion_que_compre_dolar": @cotizacion_que_compre_dolar, "cotizacion_que_vendi_dolar": cotizacion_que_vendi_dolar }
  @response_post = Faraday.post("#{BASE_URL}/inversiones/#{@cuit}/dolares", body.to_json)

  @response = Faraday.get("#{BASE_URL}/inversiones/#{@cuit}")
  expect(@response.status).to eq (200)
  @json_response = JSON.parse(@response.body)
end

Cuando("invierto $ {float} en un plazo fijo a {int} dias") do |capital_invertido, plazo_en_dias|
  body = {"capital": capital_invertido, "interes_anual": @interes_anual, "plazo_en_dias": plazo_en_dias}
  @response_post = Faraday.post("#{BASE_URL}/inversiones/#{@cuit}/plazo-fijos", body.to_json)

  @response = Faraday.get("#{BASE_URL}/inversiones/#{@cuit}")
  expect(@response.status).to eq (200)
  @json_response = JSON.parse(@response.body)
end

Cuando("vendo esas acciones a $ {float} por acción") do |valor_por_accion_vendida|
  body = {"capital": @capital_invertido, "valor_por_accion_comprada":@valor_por_accion_comprada, "valor_por_accion_vendida":valor_por_accion_vendida }
  @response_post = Faraday.post("#{BASE_URL}/inversiones/#{@cuit}/acciones", body.to_json)


  @response = Faraday.get("#{BASE_URL}/inversiones/#{@cuit}")
  expect(@response.status).to eq (200)
  @json_response = JSON.parse(@response.body)
end

Entonces("obtengo una ganancia bruta de $ {float}") do |monto_ganancia_bruta_esperada|
  ganancia_bruta = @json_response["ganancia_bruta"].to_f
  expect(ganancia_bruta).to eq monto_ganancia_bruta_esperada
end

Entonces("$ {float} de impuestos-comisiones") do |monto_impuestos_esperado|
  impuesto = @json_response["impuestos"].to_f
  expect(impuesto).to eq monto_impuestos_esperado
end

Entonces("eso resulta en una ganancia neta de $ {float}") do |monto_ganancia_neta_esperada|
  ganancia_neta = @json_response["ganancia_neta"].to_f
  expect(ganancia_neta).to eq monto_ganancia_neta_esperada
end

Entonces('obtengo un error {string}') do |mensaje|
  #json_response = JSON.parse(@response_post.body)
  expect(@response_post.status).to eq 404
  #expect(json_response['mensaje_error']).to eq mensaje
end